def setup_logger(name):
    """Return a logger with a default ColoredFormatter."""
    import logging as lg
    import coloredlogs

    logger = lg.getLogger(name)
    logger.setLevel(lg.DEBUG)

    coloredlogs.install(level='DEBUG')

    return logger


def json_to_dict(json_path):
    '''Reads JSON file and returns a dict
    '''
    import json
    import re
    import time

    with open(json_path, 'r') as raw:
        for i in range(10):
            string = raw.read()
            try:
                result = json.loads(string)   # try to parse...
                break                    # parsing worked -> exit loop
            except Exception as e:
                # "Expecting , delimiter: line 34 column 54 (char 1158)"
                # position of unexpected character after '"'
                unexp = int(re.findall(r'\(char (\d+)\)', str(e))[0])
                # position of unescaped '"' before that
                unesc = string.rfind(r'"', 0, unexp)
                string = string[:unesc] + r'\"' + string[unesc + 1:]
                # position of correspondig closing '"' (+2 for inserted '\')
                closg = string.find(r'"', unesc + 2)
                string = string[:closg] + r'\"' + string[closg + 1:]

                time.sleep(5)
    return result


def dict_to_json(jdict, json_path):
    '''Writes dict as JSON to file
    '''
    from json import dump

    with open(json_path, 'w') as raw:
        dump(jdict, raw, indent=4, sort_keys=True)
    return


def mkdir(path):
    import os.path

    if not os.path.exists(path):
        os.makedirs(path)

    return
