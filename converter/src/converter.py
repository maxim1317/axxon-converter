import os

from .utils import setup_logger, mkdir


logger = setup_logger('Converter')


class Converter(object):
    """Converts axxonsoft archive to mp4 video files using multiprocessing"""

    def __init__(self, directory, progress='progress.json'):
        self.directory = directory
        self.json = progress

    def _get_file_list(self):
        """Find all files looking like axxon records in a directory(recursively)

        Returns:
            list: list of files in a directory
        """
        import glob

        path = self.directory

        logger.info('Looking for files in "{}"'.format(path))

        files = [file for file in glob.glob(path + "/**/*._*", recursive=True)]

        logger.info('Found {} files in "{}"'.format(len(files), path))

        return files

    def _get_cam_list(self, files):
        """Creates a dictionary with files split by cams

        Args:
            files (list): list of files in a directory

        Returns:
            dict: dict, that looks like {<cam_num>: [file_1, file_2]}
        """
        cams = {}

        logger.info('Counting cams')

        for file in files:
            cam = (file.split('._')[1])
            if not cams.get(cam, False):
                logger.debug('Added camera from file "{}": {}'.format(file, cam))
                cams[cam] = [{'path': file, 'status': 'Pending'}]
            else:
                cams[cam].append({'path': file, 'status': 'Pending'})

        logger.info('Found {} cams: {}'.format(len(cams), list(cams.keys())))

        return cams

    def _save_cams(self, cams):
        """Saves cams dictionary to json for monitoring purposes

        Args:
            cams (dict): cams dict

        Returns:
            None
        """
        from .utils import dict_to_json

        dict_to_json(cams, self.json)
        return

    def _load_cams(self):
        """Loads cams dictionary from json, unused

        Returns:
            dict: cams dict
        """
        from .utils import json_to_dict

        cams = json_to_dict(self.json)
        return cams

    def _get_cores(self):
        """Gets number of CPU cores available on machine (minus one)

        Returns:
            int: number of CPU cores available on machine
        """
        import multiprocessing

        cores = multiprocessing.cpu_count()

        logger.info('Found {} cores, using {}'.format(cores, cores - 1))

        if cores > 1:
            return cores - 1
        else:
            return cores

    def _get_converter_queue(self, cams, cores):
        """Creates a queue of files for processing and fills it

        Args:
            cams (dict): cams dict
            cores (int): number of CPU cores

        Returns:
            multiprocessing.Queue: Multithread-safe queue
        """
        from multiprocessing import Queue

        queue = Queue()

        for cam in cams.keys():
            for file in cams[cam]:
                record = {
                    'camera': cam,
                    'path': file['path']
                }
                queue.put(record)

        (queue.put(True) for core in range(cores + 1))

        return queue

    def _get_monitor_queue(self):
        """Creates queue to monitor updates from workers

        Returns:
            multiprocessing.Queue: Multithread-safe queue
        """
        from multiprocessing import Queue

        queue = Queue()

        return queue

    def _spawn_monitoring(self, total, monitor_queue, cores):
        """Creates process for monitoring workers

        Args:
            total (int): Amount of files to process
            monitor_queue (multiprocessing.Queue): Queue
            cores (int): Number of CPU cores

        Returns:
            multiprocessing.Process: Monitoring process
        """
        from multiprocessing import Process

        monitor = Process(
            target=self._monitor,
            args=(
                total,
                monitor_queue,
                cores
            )
        )

        return monitor

    def _monitor(self, total, queue, cores):
        """Manages tqdm progress bar and updating progress.json

        Args:
            total (int): Amount of files to process
            queue (multiprocessing.Queue): Queue
            cores (int): Number of CPU cores

        Returns:
            TYPE: Description
        """
        from tqdm import tqdm

        progress = tqdm(total=total, desc='Converting files')

        finished = 0
        files_processed = 0
        fps = 0

        update = queue.get()
        while finished <= cores:
            if update is True:
                finished += 1
                logger.debug('Workers finished: {}'.format(finished))
                if finished == cores:
                    break
            elif update is False:
                progress.update()
            else:
                if update['status'] == 'Converted':
                    fps += int(update['fps'])
                    files_processed += 1
                    progress.set_description(
                        desc='Converted file: "{}". Current speed: {} FPS'.format(
                            update['path'], int(fps / files_processed)
                        )
                    )
                    logger.debug(
                        'Converted file: "{}". Current speed: {} FPS'.format(
                            update['path'],
                            int(fps / files_processed)
                        )
                    )
                    progress.update()
                else:
                    progress.refresh()

                cam = update['camera']
                path = update['path']

                for d in self.cams[cam]:
                    if path == d['path']:
                        self.cams[cam][self.cams[cam].index(d)]['status'] = update['status']
                        self._save_cams(self.cams)
                        break

            # if queue.empty():
            #     break
            update = queue.get()

        progress.close()
        logger.info('All {} workers has finished'.format(finished))

        return True

    def _spawn_workers(self, cores, converter_queue, monitor_queue, output_loc):
        """Creates <cores> amount of converter processes

        Args:
            cores (int): Number of CPU cores
            converter_queue (TYPE): Description
            monitor_queue (TYPE): Description
            output_loc (TYPE): Description

        Returns:
            TYPE: Description
        """
        from multiprocessing import Process

        workers = [
            Process(
                target=self._worker,
                args=(
                    i,
                    converter_queue,
                    monitor_queue,
                    output_loc
                )
            ) for i in range(cores)
        ]

        return workers

    def _worker(self, _id, converter_queue, monitor_queue, output_loc):
        """Converts video

        Args:
            _id (int): Worker ID
            converter_queue (multiprocessing.Queue): Input queue
            monitor_queue (multiprocessing.Queue): Output queue
            output_loc (str): Directory to write result

        Returns:
            None
        """
        logger.debug('Worker-{} started'.format(_id))

        if converter_queue.empty():
            logger.debug('Worker-{} finished: empty queue'.format(_id))
            monitor_queue.put(True)
            return True
        batch = converter_queue.get()

        while batch is not True:
            inp = batch['path']

            cam = batch['camera']
            temp = str(int(os.path.split(inp)[1].split('.')[0], 16)).zfill(5)
            prefix = os.path.basename(os.path.dirname(inp)).replace(' ', '_').replace('-', '_')

            filename = f'{prefix}_{temp}.mkv'

            mkdir(os.path.join(output_loc, cam))
            out = os.path.join(output_loc, cam, filename)

            if os.path.exists(out):
                pass
            else:
                batch['status'] = 'Processing'
                monitor_queue.put(batch)
                fps = self._vid_to_mp4(
                    inp=inp,
                    out=out
                )
                batch['status'] = 'Converted'
                batch['fps'] = fps
                monitor_queue.put(batch)

            if converter_queue.empty():
                logger.debug('Worker-{} finished'.format(_id))
                monitor_queue.put(True)
                return True
            batch = converter_queue.get()

        monitor_queue.put(True)
        logger.debug('Worker-{} finished'.format(_id))

        return True

    def _vid_to_mp4(self, inp, out):
        """Reads video files and converts it to mp4

        Args:
            inp (str): Path to input file
            out (str): Path to output file

        Returns:
            float: converter FPS
        """
        # import cv2
        import time
        from subprocess import Popen, DEVNULL

        write_fps = 0

        start_time = time.time()

        Popen(['ffmpeg', '-err_detect', 'ignore_err', '-i', inp, '-c', 'copy', out], stderr=DEVNULL, stdout=DEVNULL)

        end_time = time.time()

        write_fps = 1 / (end_time - start_time)
        # finally:
        #     writer.release()

        # logger.debug('Written {} frames'.format(frame_count))

        return write_fps

    def _run(self, monitor, workers):
        monitor.start()
        for worker in workers:
            worker.start()

        for worker in workers:
            worker.join()
        monitor.join()

        return True

    def convert(self, output_loc: str, cameras=None):
        """run convertation progress and put files to <output_loc>

        Args:
            output_loc (str): directory to write to

        Returns:
            None
        """
        files = self._get_file_list()
        total = len(files)
        self.cams = self._get_cam_list(files)

        mkdir(output_loc)
        self._save_cams(self.cams)

        if cameras is not None:
            for camera in cameras:
                if str(camera) not in self.cams:
                    raise Exception('No such camera: {}'.format(camera))
            for cam in cameras:
                self.cams.pop(cam, None)

        self.cores = self._get_cores()

        converter_queue = self._get_converter_queue(self.cams, self.cores)
        monitor_queue   = self._get_monitor_queue()

        monitor = self._spawn_monitoring(total=total, monitor_queue=monitor_queue, cores=self.cores)

        workers = self._spawn_workers(
            cores=self.cores,
            converter_queue=converter_queue,
            monitor_queue=monitor_queue,
            output_loc=output_loc
        )

        self._run(monitor, workers)

        return True
