import pytest
from ..src.utils import setup_logger
from ..src.converter import Converter

PATH = './converter/tests/test_folder'

FILES = [
    './converter/tests/test_folder/1/4._02',
    './converter/tests/test_folder/2/2._47'
]
FILES_LEN = 2
CAMS = {
    '02': [{'path': './converter/tests/test_folder/1/4._02', 'status': 'Pending'}],
    '47': [{'path': './converter/tests/test_folder/2/2._47', 'status': 'Pending'}]

}
CAMS_LEN = 2

logger = setup_logger('Tests')


@pytest.fixture(autouse=True, scope='module')
def module_setup_teardown():
    # import shutil

    global FILES
    global CAMS
    global FILES_LEN
    global CAMS_LEN

    # logger.info("Creating test directory")

    # # dirs = [PATH + '/1_1', PATH + '/1_2', PATH + '/1_3']

    # # for path in dirs:
    # #     mkdir(path)

    # #     for i in range(1, 4):
    # #         cam = str(i) + str(i)
    # #         file = path + '/' + str(i) + '._' + str(cam)
    # #         try:
    # #             open(file, 'r')
    # #         except IOError:
    # #             open(file, 'w')

    # #         FILES.append(file)
    # #         FILES_LEN += 1
    # #         if not CAMS.get(cam, False):
    # #             CAMS_LEN += 1
    # #             CAMS[cam] = [{'path': file, 'status': 'Pending'}]
    # #         else:
    # #             CAMS[cam].append({'path': file, 'status': 'Pending'})
    # logger.info("Test directory created")

    yield

    # logger.info("Removing test directory")
    # shutil.rmtree(PATH)
    # logger.info("Test directory removed")


def test_get_file_list():

    global FILES
    global FILES_LEN

    converter = Converter(PATH)

    converter_files = converter._get_file_list()

    assert (set(converter_files) == set(FILES)) is True
    assert (len(converter_files) == FILES_LEN) is True

    return


def test_get_cam_list():

    global CAMS
    global CAMS_LEN

    converter = Converter(PATH)

    converter_cams = converter._get_cam_list(FILES)

    assert (converter_cams == CAMS) is True
    assert (len(converter_cams.keys()) == CAMS_LEN)

    return


def test_converter():

    converter = Converter(PATH)

    assert converter.convert('output') is True
