#! /bin/bash

docstr-coverage --skipmagic --verbose=1 converter/ && \
pytest -s --cov=converter converter/tests/ && coverage html