#!/bin/sh

mkinit --inplace --noattrs converter/ > /dev/null
mkinit --inplace --noattrs converter/src > /dev/null
mkinit --inplace --noattrs converter/tests > /dev/null

# docker-compose down
# docker network create --subnet 10.5.0.0/24 converternet 2>/dev/null || true