FROM jjanzic/docker-python3-opencv

RUN apt update && apt install -y  ffmpeg

RUN pip3 install --no-cache \
    pylint \
    pytest \
    pytest-cov \
    pytest-sugar \
    coverage-badge \
    docstr-coverage \
    mkinit \
    tqdm \
    coloredlogs

COPY prepare.sh /axxon-converter/
COPY run.sh /axxon-converter/
COPY run.py /axxon-converter/
COPY run_tests.py /axxon-converter/

WORKDIR /axxon-converter

COPY converter/ converter/

CMD bash run.sh
